# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
# pylint: disable=trailing-whitespace
# pylint: disable=useless-import-alias
# pylint: disable=missing-module-docstring
import contextlib
import io
import logging
import os
import tempfile
import unittest
import pytest as pytest
from translate import translator
import machine


@pytest.mark.golden_test("tests/*.yml")
def test_whole_by_golden(golden, caplog):
    caplog.set_level(logging.DEBUG)

    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.bf")
        input_stream = os.path.join(tmpdirname, "input.txt")
        target = os.path.join(tmpdirname, "target.o")

        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden["input"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source, target])
            machine.main([target, input_stream])

        assert stdout.getvalue() == golden.out["output"]
        assert caplog.text == golden.out["log"]
