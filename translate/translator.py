#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=import-error
# pylint: disable=missing-module-docstring

import sys

from isa import Instruction, opcode, operandsType
from translate.utils import is_section_instr, is_label_instr, is_reg, number_to_bin, get_word_value, \
    bin_line_to_ascii_code, ascii_code_to_bin_line, bin_to_number, get_key


def detranslate(code_file):
    data: list[str] = []
    instruction: list[str] = []
    with open(code_file, encoding="utf-8") as file:
        file_text: str = file.read()
        file_text = file_text.strip()
    code = ascii_code_to_bin_line(file_text)
    number_of_vars = bin_to_number(code[:16], False)
    code = code[16:]
    for i in range(number_of_vars):
        data.append(code[:32])
        code = code[32:]
    while len(code) > 0:
        operands_type = get_key(operandsType, bin_to_number(code[4:8], False))
        if operands_type == "NONE":
            instruction.append(code[:8])
            code = code[8:]
        elif operands_type == "REG":
            instruction.append(code[:10])
            code = code[10:]
        elif operands_type == "CONST" or operands_type == "MEM":
            instruction.append(code[:24])
            code = code[24:]
        elif operands_type == "REG_REG":
            instruction.append(code[:12])
            code = code[12:]
        elif operands_type == "MEM_REG" or operands_type == "REG_MEM" or operands_type == "REG_CONST" or operands_type == "CONST_REG":
            instruction.append(code[:26])
            code = code[26:]
        else:
            instruction.append(code[:40])
            code = code[40:]
    return data, instruction


def splitting(asm_code):
    asm_code = asm_code.strip()
    raw_code = asm_code.split('\n')
    raw_code: list[str] = list(map(lambda it: it.strip(), raw_code))

    while True:
        try:
            raw_code.remove('')
        except ValueError as e:
            break
    return raw_code


def labelling(raw_code):
    text_labels: dict = {}
    data_labels: dict = {}
    current_section = ''
    data_pointer = 0
    text_pointer = 0

    for i in range(len(raw_code)):
        if is_section_instr(raw_code[i]):
            current_section = raw_code[i]
            continue

        if is_label_instr(raw_code[i]):
            if current_section == 'section .data':
                data_labels[raw_code[i][:-1]] = data_pointer
                data_pointer += 1
            elif current_section == 'section .text':
                text_labels[raw_code[i][:-1]] = text_pointer
        else:
            text_pointer += 1
    return text_labels, data_labels


def filling(text_labels, data_labels, raw_code):
    list_of_instructions: list[Instruction] = []

    if raw_code.index('section .text') < raw_code.index('section .data') and len(text_labels) > 0 and len(
            data_labels) > 0:
        text = raw_code[:raw_code.index('section .data')][1:]
        data = raw_code[raw_code.index('section .data'):][1:]
    else:
        data = raw_code[:raw_code.index('section .text')][1:]
        text = raw_code[raw_code.index('section .text'):][1:]

    count_of_vars = 0

    for i in range(len(text)):
        if is_label_instr(text[i]):
            count_of_vars += 1
            continue
        command = text[i].split()
        operation_opcode = command[0].upper()
        operands_opcode = ''
        operands: list[int] = []
        if len(command) == 3:
            command[1] = command[1][:-1]
        elif len(command) == 1:
            operands_opcode = 'NONE_'
        for n in range(len(command) - 1):
            if data_labels.get(command[n + 1]) or data_labels.get(command[n + 1]) == 0:
                operands_opcode = operands_opcode + 'MEM_'
                operands.append(data_labels[command[n + 1]])
            if text_labels.get(command[n + 1]) or text_labels.get(command[n + 1]) == 0:
                operands_opcode = operands_opcode + 'MEM_'
                if text_labels.get(command[n + 1]) > i - count_of_vars:
                    operands.append(text_labels[command[n + 1]] - (i - count_of_vars))
                else:
                    operands.append(int('-' + str((i - count_of_vars) - text_labels[command[n + 1]])))
                continue
            if is_reg(command[n + 1]):
                operands_opcode = operands_opcode + 'REG_'
                operands.append(command[n + 1][-1])
                continue
            try:
                if int(command[n + 1]):
                    operands_opcode = operands_opcode + 'CONST_'
                    operands.append(int(command[n + 1]))
            except ValueError:
                continue
        list_of_instructions.append(Instruction(operation_opcode, operands_opcode[:-1], operands))
    return count_of_vars, data, list_of_instructions


def translate(asm_code):
    raw_code: list[str] = splitting(asm_code)
    text_labels, data_labels = labelling(raw_code)
    count_of_vars, data, list_of_instructions = filling(text_labels, data_labels, raw_code)

    code = []
    code.append(number_to_bin(int(len(data)/2), 16))
    for i in range(len(data)):
        if is_label_instr(data[i]):
            continue
        code.append(number_to_bin(int(get_word_value(data[i])), 32))
    instr = ''
    if text_labels["start"] != 0:
        instr = '10100011' + str(number_to_bin(text_labels["start"] + 1, 16))
        code.append(instr)
    for instruction in list_of_instructions:
        instr = ''
        instr = instr + str(number_to_bin(opcode[instruction.operation_opcode], 4))
        instr = instr + str(number_to_bin(operandsType[instruction.operands_opcode], 4))

        for i in range(len(instruction.operands)):
            instr = instr + str(number_to_bin(instruction.operands[i], 16))
        code.append(instr)
    return code


def ascii_translate(code):
    ascii_code = []
    for i in range(len(code)):
        ascii_code.append(bin_line_to_ascii_code(code[i]))
    return ascii_code


def main(args):
    assert len(args) == 2, "Wrong arguments: translator.py <asm_file> <target>"

    asm_code, bin_code = args

    with open(asm_code, "rt", encoding="utf-8") as file:
        asm_code = file.read()

    code = translate(asm_code)
    ascii_code = ascii_translate(code)

    with open(bin_code, "w", encoding="utf-8") as file:
        file.write(''.join(ascii_code))


if __name__ == '__main__':
    sys.path.append('.')
    main(sys.argv[1:])
