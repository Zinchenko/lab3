import re


def is_section_instr(instr: str) -> bool:
    return re.match(r'^section\s+\.[a-z]+$', instr) is not None


def is_label_instr(instr: str) -> bool:
    return re.match(r'^[a-z]+:$', instr) is not None


def get_word_value(instr: str) -> str:
    return re.findall(r'(-?[0-9]+)', instr).pop(0)


def is_reg(instr: str) -> bool:
    return re.match(r'^reg[0-4]$', instr) is not None


def get_word_value(instr: str) -> str:
    return re.findall(r'(-?[0-9]+)', instr).pop(0)


def number_to_bin(value: int, word_length: int) -> str:
    if value < 0:
        value = abs(value) - 1
        number = bin(abs(value)).replace('0b', '')
        number = (word_length - len(number)) * '0' + number
        new_number: str = ''
        for c in number:
            if c == '0':
                new_number += '1'
            else:
                new_number += '0'
            number = new_number
    else:
        number = bin(abs(value)).replace('0b', '')
        number = (word_length - len(number)) * '0' + number
    return number


def bin_to_number(value: str, extra_code: bool) -> int:
    if not extra_code or value[0] == '0':
        return int('0b' + value, 2)
    abs_value_bin: str = ''
    for i in value:
        if i == '0':
            abs_value_bin += '1'
        else:
            abs_value_bin += '0'
    abs_value: int = int('0b' + abs_value_bin, 2)
    return (abs_value + 1) * -1


def bin_line_to_ascii_code(line: str):
    result = ''
    for i in range(len(line)//8):
        code = line[:8]
        code = bin_to_number(code, False)
        result += chr(code)
        line = line[8:]
    return result


def read_bin_code_from_file(filename: str, word_size: int) -> list[str]:
    lines: list[str] = []
    with open(filename, encoding="utf-8") as file:
        file_text: str = file.read()
        file_text = file_text.strip()
        # lines = file_text.split('\n')
        if len(file_text) % word_size != 0:
            assert False, "ILLEGAL BIN NUMBER = {}".format(len(file_text))
        for _ in range(0, int(len(file_text) / word_size)):
            lines.append(file_text[:word_size])
            file_text = file_text[word_size:]
    return lines


def get_key(d, value):
    for k, v in d.items():
        if v == value:
            return k


def ascii_code_to_bin_line(file_text):
    result = ''
    for i in range(len(file_text)):
        code = ord(file_text[:1])
        file_text = file_text[1:]
        code = number_to_bin(code, 8)
        result += code
    return result
