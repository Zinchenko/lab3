#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=consider-using-f-string
# pylint: disable=import-error
# pylint: disable=too-few-public-methods

import logging
import sys

from translate.translator import detranslate
from translate.utils import get_key, bin_to_number, number_to_bin
from isa import opcode, operandsType


class Memory:
    def __init__(self, mem_size, data, instruction):
        self.memory = [0] * mem_size
        self.instruction = instruction
        self.data = data

    def get(self, address: int):
        return self.memory[address]

    def set(self, address: int, value: int):
        self.memory[address] = value


class RegisterController:
    def __init__(self):
        self.PC: int = 0
        self.MAR: int = 0
        self.MDR: int = 0
        self.CIR: int = 0
        self.AC: int = 0
        self.registers: dict = {"REG0": 0, "REG1": 0, "REG2": 0, "REG3": 0}


class Alu:
    def __init__(self):
        self.left: int = 0
        self.right: int = 0
        self.operations: dict = {
            "DIV": lambda left, right: left / right,
            "MOD": lambda left, right: left % right,
            "CMP": lambda left, right: left - right,
            "ADD": lambda left, right: left + right,
            "SUB": lambda left, right: right - left,
            "MUL": lambda left, right: left * right,
        }
        self.flags: dict = {"N": False, "Z": False, "V": False, "C": False}


class DataPath:
    def __init__(self, input_buffer: list, memory):
        self.register_controller: RegisterController = RegisterController()
        self.memory: Memory = memory
        self.alu: Alu = Alu()
        self.output_buffer = []
        self.input_buffer = input_buffer

    def set_flags(self, result: int):
        self.alu.flags["N"] = result < 0
        self.alu.flags["Z"] = result == 0
        self.alu.flags["V"] = (self.alu.left > 0 and self.alu.right > 0 and result < 0) | \
                              (self.alu.left < 0 and self.alu.right < 0 and result > 0)
        self.alu.flags["C"] = result > (2 ** 32 - 1) | result < -(2 ** 32)

    def execute_alu(self, operation, is_tmp: bool):
        res = self.alu.operations[operation](self.alu.left, self.alu.right)
        while res > 2 ** 32 - 1:
            res = -(2 ** 32) + (res - (2 ** 32 - 1))
        while res < -(2 ** 32):
            res = (2 ** 32 - 1) - (res + 2 ** 32)
        if not is_tmp:
            self.register_controller.MDR = res
            self.register_controller.AC = res
        self.set_flags(res)
        self.alu.right = 0
        self.alu.left = 0


class ControlUnit:
    def __init__(self, data_path):
        self.data_path: DataPath = data_path
        self._tick: int = 0

    def __repr__(self):
        state = "{{TICK: {}, AC: {}, MAR: {}, MDR: {}, PC: {}, CIR: {}}}".format(
            self._tick,
            self.data_path.register_controller.AC,
            self.data_path.register_controller.MAR,
            self.data_path.register_controller.MDR,
            self.data_path.register_controller.PC,
            self.data_path.register_controller.CIR,
        )
        return "{}".format(state)

    def tick(self):
        self._tick += 1

    def current_tick(self):
        return self._tick

    def decode_and_execute_instruction(self, datapath):
        datapath.register_controller.CIR = datapath.memory.get(datapath.register_controller.PC)
        self.tick()
        operation = get_key(opcode, bin_to_number(datapath.register_controller.CIR[:4], False))
        if operation == "EXIT":
            raise StopIteration()
        elif operation == "BEQ":
            if datapath.alu.flags["Z"]:
                offset = bin_to_number(datapath.register_controller.CIR[8:], True)
                self.tick()
                datapath.register_controller.PC += offset
                self.tick()
            else:
                datapath.register_controller.PC += 1
                self.tick()
        elif operation == "BNE":
            if datapath.alu.flags["Z"]:
                datapath.register_controller.PC += 1
                self.tick()
            else:
                offset = bin_to_number(datapath.register_controller.CIR[8:], True)
                self.tick()
                datapath.register_controller.PC += offset
                self.tick()
        elif operation == "JMP":
            offset = bin_to_number(datapath.register_controller.CIR[8:], True)
            self.tick()
            datapath.register_controller.PC += offset
            self.tick()
        elif operation == "PRINT":
            datapath.register_controller.MAR = bin_to_number(datapath.register_controller.CIR[8:24], True)
            self.tick()
            datapath.register_controller.MDR = datapath.memory.get(datapath.register_controller.MAR)
            self.tick()
            datapath.register_controller.PC += 1
            datapath.output_buffer.append(datapath.register_controller.MDR)
        elif operation == "READ":
            datapath.register_controller.MAR = bin_to_number(datapath.register_controller.CIR[8:24], True)
            self.tick()
            if len(datapath.input_buffer) > 0:
                datapath.memory.set(datapath.register_controller.MAR, number_to_bin(ord(datapath.input_buffer[0]), 8))
                self.tick()
                datapath.input_buffer = datapath.input_buffer[1:]
                self.tick()
                datapath.register_controller.PC += 1
                self.tick()
            else:
                raise StopIteration()
        elif operation == "CMP":
            operands_type = get_key(operandsType, bin_to_number(datapath.register_controller.CIR[4:8], False))
            if operands_type == "MEM_MEM":
                self.tick()
                datapath.register_controller.MAR = bin_to_number(datapath.register_controller.CIR[8:24], True)
                self.tick()
                datapath.register_controller.MDR = datapath.memory.get(datapath.register_controller.MAR)
                op1 = bin_to_number(datapath.register_controller.MDR, True)
                self.tick()
                datapath.register_controller.MAR = bin_to_number(datapath.register_controller.CIR[24:40], True)
                self.tick()
                datapath.register_controller.MDR = datapath.memory.get(datapath.register_controller.MAR)
                op2 = bin_to_number(datapath.register_controller.MDR, True)
                datapath.alu.left = op1
                datapath.alu.right = op2
                datapath.execute_alu("SUB", True)
                datapath.register_controller.PC += 1
        elif operation == "ADD" or operation == "MOD":
            operands_type = get_key(operandsType, bin_to_number(datapath.register_controller.CIR[4:8], False))
            if operands_type == "CONST_MEM":
                op1 = bin_to_number(datapath.register_controller.CIR[8:24], True)
                self.tick()
                datapath.register_controller.MAR = bin_to_number(datapath.register_controller.CIR[24:40], True)
                self.tick()
                datapath.register_controller.MDR = datapath.memory.get(datapath.register_controller.MAR)
                self.tick()
                op2 = bin_to_number(datapath.register_controller.MDR, True)
                self.tick()
            elif operands_type == "MEM_CONST":
                datapath.register_controller.MAR = bin_to_number(datapath.register_controller.CIR[8:24], False)
                self.tick()
                datapath.register_controller.MDR = datapath.memory.get(datapath.register_controller.MAR)
                self.tick()
                op1 = bin_to_number(datapath.register_controller.MDR, True)
                self.tick()
                op2 = bin_to_number(datapath.register_controller.CIR[24:40], True)
                self.tick()
            elif operands_type == "MEM_MEM":
                datapath.register_controller.MAR = bin_to_number(datapath.register_controller.CIR[24:40], True)
                self.tick()
                datapath.register_controller.MDR = datapath.memory.get(datapath.register_controller.MAR)
                self.tick()
                op2 = bin_to_number(datapath.register_controller.MDR, True)
                datapath.register_controller.MAR = bin_to_number(datapath.register_controller.CIR[8:24], True)
                self.tick()
                datapath.register_controller.MDR = datapath.memory.get(datapath.register_controller.MAR)
                self.tick()
                op1 = bin_to_number(datapath.register_controller.MDR, True)
            datapath.alu.left = op1
            datapath.alu.right = op2
            datapath.execute_alu(operation, False)
            self.tick()
            if operation != "MOD":
                datapath.memory.set(datapath.register_controller.MAR,
                                    number_to_bin(datapath.register_controller.AC, 32))
            self.tick()
            datapath.register_controller.PC += 1
        else:
            datapath.register_controller.PC += 1


def simulation(data, instruction, input_buffer, limit):
    instr_counter = 0
    memory = Memory(256, 0, len(data))
    datapath = DataPath(input_buffer, memory)
    control_unit = ControlUnit(datapath)
    control_unit.data_path.register_controller.PC = len(data)
    for i in range(len(data)):
        control_unit.data_path.memory.set(i, data[i])

    for i in range(len(instruction)):
        control_unit.data_path.memory.set(len(data) + i, instruction[i])

    try:
        while True:
            assert limit > instr_counter, "TOO LONG EXECUTION"
            control_unit.decode_and_execute_instruction(datapath)
            instr_counter += 1
            logging.debug('%s', control_unit)
    except EOFError:
        logging.warning("Input buffer is empty!")
    except StopIteration:
        pass
    if len(datapath.output_buffer) > 1:
        result = ''
        for i in range(len(datapath.output_buffer)):
            result += (chr(bin_to_number(datapath.output_buffer[i], False)))
        print(result)
    elif len(datapath.output_buffer) == 1:
        print(bin_to_number(datapath.output_buffer[0], True))


def main(args):
    assert len(args) == 2, "Wrong arguments: machine.py <code_file> <input_file>"
    code_file, input_file = args
    input_buffer = []
    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        for char in input_text:
            input_buffer.append(char)
    data, instruction = detranslate(code_file)
    simulation(data, instruction, input_buffer, limit=10000)


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])

