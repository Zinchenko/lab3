# pylint: disable=missing-class-docstring
# pylint: disable=too-few-public-methods

opcode: dict = {
    "BEQ": 0b0001,
    "BNE": 0b0010,
    "MOV": 0b0011,
    "DIV": 0b0100,
    "MUL": 0b0101,
    "MOD": 0b0110,
    "CMP": 0b0111,
    "ADD": 0b1000,
    "SUB": 0b1001,
    "JMP": 0b1010,
    "EXIT": 0b1100,
    "READ": 0b1101,
    "PRINT": 0b1111
}

operandsType: dict = {
    "NONE": 0b0001,
    "REG": 0b0010,
    "MEM": 0b0011,
    "CONST": 0b0100,
    "REG_REG": 0b0101,
    "REG_MEM": 0b0110,
    "MEM_REG": 0b0111,
    "MEM_MEM": 0b1000,
    "REG_CONST": 0b1001,
    "CONST_REG": 0b1010,
    "MEM_CONST": 0b1011,
    "CONST_MEM": 0b1100,
    "CONST_CONST": 0b1101
}


class Instruction:
    def __init__(self, operation_opcode: str, operands_opcode: str, operands: list[int]):
        self.operation_opcode: str = operation_opcode
        self.operands_opcode: str = operands_opcode
        self.operands: list[int] = operands
