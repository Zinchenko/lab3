# Assembler. Транслятор и модель

- Зинченко Константин Сергеевич, гр. P33121
- `asm | cisc | neum | hw | instr | binary | stream | port | prob1`

# Язык программирования

``` bnf
program ::= line "\n" program

line ::= | label | comment | command | section

label ::= name ":"

comment ::= ; <any sequence not containing ';'>

command ::= "\t" operation_2_args " " operand ", " operand |
            "\t" operation_1_arg " " operand |
            "\t" operation_0_args |

section ::= "section ." section_name

section_name ::= "text" | "data"

operation_2_args ::= "add" | "sub" | "mov" | "div" | "mod" | "cmp"

operation_1_arg ::= "beq" | "bne" | "jmp" | "word" | "print" | "read"

operation_0_args ::= "exit"

operand ::= name | number

name ::= [a-z_]+
          
number ::= [-2^32; 2^32 - 1]

register ::= [REG0, REG1, REG2, REG3]

```

Код выполняется последовательно. Операции:

- `add <arg1> <arg2>` -- прибавить к первому аргументу второй
- `sub <arg1> <arg1>` -- вычесть из первого аргумента второй
- `mov <arg1> <arg2>` -- скопировать значение из второго аргумента в первый
- `div <arg1> <arg2>` -- получить целую часть от деления первого аргумента на последующие аргументы
- `mod <arg1> <arg2>` -- получить остаток от деления первого аргумента на последующие аргументы
- `mul <arg1> <arg2>` -- получить произведение первого аргумента на последующие аргументы
- `cmp <arg1> <arg2>` -- получить результат сравнения первого аргумента со вторым (0, если аргументы равны)
- `bne <label>` -- если не выставлен zero flag, перейти на аргумент-метку
- `beq <label>` -- если выставлен zero flag, перейти на аргумент-метку
- `jmp <label>` -- безусловный переход на аргумент-метку
- `print <arg>` -- распечатать в поток вывода значение из аргумента
- `read <arg>` -- прочитать в аргумент значение из потока ввода
- `exit` -- завершить выполнение программы

Поддерживаемые аргументы:
- регистры `REG0`,`REG1`, `REG2`, `REG3`
- название **объявленной** метки
- число в диапазоне [-2^32; 2^32 - 1]

Дополнительные конструкции:
- `.text` - объявление секции кода
- `.data` - объявление секции данных
- `<label>:` - метки для переходов / названия переменных


## Организация памяти

Память данных и команд совмещены. Начальный адрес данных, команд и размер памяти задаётся в глобальных переменных в `machine.py`.

### Набор инструкции

| Syntax                  | Ticks | 
|:------------------------|:------|
| `add/sub <arg1> <arg2>` | 7-9  | 
| `mov <arg1> <arg2>`     | 4     |
| `cmp <arg> <arg>`       | 5-7  |
| `beq/bne/jmp <arg>`     | 1-2   |
| `exit`                  | 0     | 
| `print <arg>`           | 2     | 
| `read <arg>`            | 2     |



Типы данные в модуле [isa](./isa.py), где:

* `opcode` -- перечисление кодов операций;
* `operandsType` -- тип адресации операнда
* `Instruction` -- класс для удобного представления инструкции.

## Транслятор

Интерфейс командной строки: `translator.py <input_file.asm> <target_file.json>"`

Реализовано в модуле: [translator](translate/translator.py)

Пример кода prob1:

```
section .text
sum:
	add res, counter
	jmp end
start:
	add counter, 1
	mod counter, 3
	beq sum
	mod counter, 5
	beq sum
end:
	cmp counter, max
	bne start
	print res
	exit
section .data
max:
	word 999
res:
	word 0
counter:
	word 0

```


## Модель процессора

Реализовано в модуле: [machine](machine.py).

![https://gitlab.se.ifmo.ru/Zinchenko/cslab3/DIAGRAM.jpg](/DIAGRAM.jpg "Схема DataPath и ControlUnit")

Регистры:

- `PC` -- для указания на текущую выполняющуюся команду
- `MAR` -- для указания адреса, по которому осуществляется вызов к памяти
- `AC` -- аккумулятор
- `MDR` -- для данных полученных из памяти
- `CIR` -- инструкция которая выполняется в данный момент
- `REG0, REG1, REG2, REG3` -- для манипуляции данными


## Апробация

В качестве тестов использовано 4 алгоритма:

1. [hello](tests/hello.asm)
2. [cat](tests/cat.asm)
3. [prob1](tests/prob1.asm)


Интеграционные тесты реализованы тут: [integration_test](integration_test.py)

CL:
```yaml
lab3-example:
  stage: test
  image:
    name: python-tools
    entrypoint: [""]
  script:
    - pip install pytest-golden
    - true && python3 -m pytest . -vvv --update-goldens
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501,W503,W293,E303
    - find . -type f -name "*.py" | xargs -t pylint
```


Пример использования и журнал работы процессора на примере `cat`:

``` commandline
DEBUG:root:{TICK: 5, AC: 0, MAR: 0, MDR: 0, PC: 2, CIR: 110100110000000000000000}
DEBUG:root:{TICK: 8, AC: 0, MAR: 0, MDR: 01101001, PC: 3, CIR: 111100110000000000000000}
DEBUG:root:{TICK: 11, AC: 0, MAR: 0, MDR: 01101001, PC: 1, CIR: 101000111111111111111110}
DEBUG:root:{TICK: 16, AC: 0, MAR: 0, MDR: 01101001, PC: 2, CIR: 110100110000000000000000}
DEBUG:root:{TICK: 19, AC: 0, MAR: 0, MDR: 01101110, PC: 3, CIR: 111100110000000000000000}
DEBUG:root:{TICK: 22, AC: 0, MAR: 0, MDR: 01101110, PC: 1, CIR: 101000111111111111111110}
DEBUG:root:{TICK: 27, AC: 0, MAR: 0, MDR: 01101110, PC: 2, CIR: 110100110000000000000000}
DEBUG:root:{TICK: 30, AC: 0, MAR: 0, MDR: 01110000, PC: 3, CIR: 111100110000000000000000}
DEBUG:root:{TICK: 33, AC: 0, MAR: 0, MDR: 01110000, PC: 1, CIR: 101000111111111111111110}
DEBUG:root:{TICK: 38, AC: 0, MAR: 0, MDR: 01110000, PC: 2, CIR: 110100110000000000000000}
DEBUG:root:{TICK: 41, AC: 0, MAR: 0, MDR: 01110101, PC: 3, CIR: 111100110000000000000000}
DEBUG:root:{TICK: 44, AC: 0, MAR: 0, MDR: 01110101, PC: 1, CIR: 101000111111111111111110}
DEBUG:root:{TICK: 49, AC: 0, MAR: 0, MDR: 01110101, PC: 2, CIR: 110100110000000000000000}
DEBUG:root:{TICK: 52, AC: 0, MAR: 0, MDR: 01110100, PC: 3, CIR: 111100110000000000000000}
DEBUG:root:{TICK: 55, AC: 0, MAR: 0, MDR: 01110100, PC: 1, CIR: 101000111111111111111110}
input
```
